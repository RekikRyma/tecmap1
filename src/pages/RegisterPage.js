import React, { useState } from 'react';
import axios from 'axios';

const RegisterPage = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [username, setUsername] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  const handleRegister = async (e) => {
    e.preventDefault();
    setErrorMessage('');

    try {
      const response = await axios.post('https://api.joeleprof.com/tec-map/auth/register', {
        email,
        password,
        username,
      });


      localStorage.setItem('token', response.data.token);

     
      console.log('Inscription réussie !');

    } catch (error) {
      
      if (error.response && error.response.status === 409) {
        setErrorMessage('Cette adresse e-mail est déjà utilisée par un autre utilisateur.');
      } else {
        setErrorMessage('Une erreur est survenue lors de l\'inscription.');
      }
    }
  };

  return (
    <div >
      {/* Navbar */}
      <nav >
      <ul >
          <li ><a href="/">Accueil</a></li>
          <li ><a href="/LoginPage">Se connecter</a></li>
          <li ><a href="/User">Gestion</a></li>
          <li ><a href="/friends">Friends</a></li>
          <li ><a href="/UsersList">Utilisateurs</a></li>
          <li ><a href="/RegisterPage">Inscription</a></li>
        </ul>
      </nav>

      <div >
        <h2 >Inscription</h2>
        {errorMessage && <p >{errorMessage}</p>}
        <form onSubmit={handleRegister} >
          <div>
            <label>Email:</label>
            <input
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
          </div>
          <div>
            <label>Mot de passe:</label>
            <input
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
          </div>
          <div>
            <label>Nom d'utilisateur:</label>
            <input
              type="text"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
              required
            />
          </div>
          <button type="submit">S'inscrire</button>
        </form>
      </div>
    </div>
  );
};

export default RegisterPage;
