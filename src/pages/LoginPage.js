import React, { useState } from 'react';
import axios from 'axios';

const LoginPage = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  const handleLogin = async (e) => {
    e.preventDefault();
    setErrorMessage('');
    try {
      const response = await axios.post('https://api.joeleprof.com/tec-map/auth/login', {
        email,
        password,
      });
      // Gérer la réponse réussie, stocker le token dans l'état global de l'application
      console.log('Token d\'authentification : ', response.data.token);
    } catch (error) {
      // Gérer les erreurs de connexion et afficher le message approprié
      if (error.response && error.response.status === 401) {
        setErrorMessage('Adresse e-mail ou mot de passe incorrect.');
      } else {
        setErrorMessage('Une erreur est survenue lors de la connexion.');
      }
    }
  };

  return (
    <div >
       <nav >
       <ul >
          <li ><a href="/">Accueil</a></li>
          <li ><a href="/LoginPage">Se connecter</a></li>
          <li ><a href="/User">Gestion</a></li>
          <li ><a href="/friends">Friends</a></li>
          <li ><a href="/UsersList">Utilisateurs</a></li>
          <li ><a href="/RegisterPage">Inscription</a></li>
        </ul>
      </nav>
      <h2 >Connexion</h2>
      {errorMessage && <p >{errorMessage}</p>}
      <form onSubmit={handleLogin}>
        <div >
          <label>Email:</label>
          <input
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
            style={styles.input}
          />
        </div>
        <div >
          <label>Mot de passe:</label>
          <input
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
            style={styles.input}
          />
        </div>
        <button type="submit" >Se connecter</button>
      </form>
    </div>
  );
};

export default LoginPage;
