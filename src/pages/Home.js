import React from 'react';

const Home = () => {
  return (
    <div >
      
      <nav >
        <ul >
          <li ><a href="/">Accueil</a></li>
          <li ><a href="/LoginPage">Se connecter</a></li>
          <li ><a href="/User">Gestion</a></li>
          <li ><a href="/friends">Friends</a></li>
          <li ><a href="/UsersList">Utilisateurs</a></li>
          <li ><a href="/RegisterPage">Inscription</a></li>
        </ul>
      </nav>

      <div >
        <h2 >Bienvenue sur Tec map</h2>
        <p >ACCEUIL</p>
      </div>
    </div>
  );
};

export default Home;
