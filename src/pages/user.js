import React, { useState, useEffect } from 'react';
import axios from 'axios';

const UserProfile = () => {
  const [user, setUser] = useState({});
  const [newUsername, setNewUsername] = useState('');
  const [newEmail, setNewEmail] = useState('');
  const [error, setError] = useState('');

  useEffect(() => {
    const fetchUserProfile = async () => {
      try {
        const response = await axios.get('https://api.joeleprof.com/tec-map/me', {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`,
          },
        });
        setUser(response.data);
      } catch (error) {
        setError('Une erreur est survenue lors de la récupération des informations de l\'utilisateur.');
      }
    };

    fetchUserProfile();
  }, []);

  const handleUpdateUser = async () => {
    try {
      await axios.put('https://api.joeleprof.com/tec-map/me',
        { username: newUsername, email: newEmail },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`,
          },
        }
      );
      // Mettre à jour les informations de l'utilisateur après la modification
      console.log('Utilisateur mis à jour avec succès !');
    } catch (error) {
      setError('Une erreur est survenue lors de la mise à jour des informations de l\'utilisateur.');
    }
  };


  const handleDeleteUser = async () => {
    try {
      await axios.delete('https://api.joeleprof.com/tec-map/me', {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      });
      console.log('Compte utilisateur supprimé avec succès !');
    } catch (error) {
      setError('Une erreur est survenue lors de la suppression du compte utilisateur.');
    }
  };

  return (
    <div>
      <h2 >Profil de l'utilisateur</h2>
      {error && <p style={styles.error}>{error}</p>}
      <div >
        <h3>Informations de l'utilisateur</h3>
        <p><strong>Nom d'utilisateur:</strong> {user.username}</p>
        <p><strong>Email:</strong> {user.email}</p>
      </div>
      <div >
        <h3>Modifier les informations de l'utilisateur</h3>
        <input
          type="text"
          value={newUsername}
          onChange={(e) => setNewUsername(e.target.value)}
          placeholder="Nouveau nom d'utilisateur"
          style={styles.input}
        />
        <input
          type="email"
          value={newEmail}
          onChange={(e) => setNewEmail(e.target.value)}
          placeholder="Nouvelle adresse e-mail"
          style={styles.input}
        />
        <button onClick={handleUpdateUser} >Enregistrer les modifications</button>
      </div>
      <div >
        <h3>Réinitialiser le pointage</h3>
        <button onClick={handleResetScore} >Réinitialiser le pointage</button>
      </div>
      <div >
        <h3>Supprimer le compte</h3>
        <button onClick={handleDeleteUser} >Supprimer le compte</button>
      </div>
    </div>
  );
};

export default UserProfile;
