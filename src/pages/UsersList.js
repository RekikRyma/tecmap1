import React, { useState, useEffect } from 'react';
import axios from 'axios';

const UsersList = () => {
  const [users, setUsers] = useState([]);
  const [error, setError] = useState('');
  const [friends, setFriends] = useState([]); 

  useEffect(() => {
    const fetchUsers = async () => {
      try {
        const response = await axios.get('https://api.joeleprof.com/tec-map/users', {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`,
          },
        });
        setUsers(response.data.data);
      } catch (error) {
        if (error.response && error.response.status === 401) {
          setError('Vous devez être connecté pour accéder à la liste des utilisateurs.');
        } else {
          setError('Une erreur est survenue lors de la récupération de la liste des utilisateurs.');
        }
      }
    };

    fetchUsers();
  }, []);

  const handleAddFriend = async userId => {
    try {
      await axios.post(`https://api.joeleprof.com/tec-map/friends/${userId}`, null, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      });
      
      const response = await axios.get('hƩps://api.joeleprof.com/tec-map/friends', {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      });
      setFriends(response.data.data);
    } catch (error) {
      if (error.response && error.response.status === 401) {
        setError('Vous devez être connecté pour ajouter un ami.');
      } else {
        setError('Une erreur est survenue lors de l\'ajout de l\'ami.');
      }
    }
  };

  
  const handleRemoveFriend = async userId => {
    try {
      await axios.delete(`https://api.joeleprof.com/tec-map /friends/${userId}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      });
      // Refresh friends list
      const response = await axios.get('https://api.joeleprof.com/tec-map/friends', {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      });
      setFriends(response.data.data);
    } catch (error) {
      if (error.response && error.response.status === 401) {
        setError('Vous devez être connecté pour supprimer un ami.');
      } else {
        setError('Une erreur est survenue lors de la suppression de l\'ami.');
      }
    }
  };

  return (
    <div >
      <h2 >Liste des utilisateurs</h2>
      {error ? <p >{error}</p> : (
        <ul >
          {users.map(user => (
            <li key={user.id} >
              <span>{user.username}</span> (Email: {user.email})
              <div >
                {!friends.find(friend => friend.id === user.id) && (
                  <button onClick={() => handleAddFriend(user.id)}>Ajouter Ami</button>
                )}
                {friends.find(friend => friend.id === user.id) && (
                  <button style={styles.removeButton} onClick={() => handleRemoveFriend(user.id)}>Supprimer Ami</button>
                )}
              </div>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

export default UsersList;
