import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "./pages/Home";
import LoginPage from "./pages/LoginPage";
 import UsersList from "./pages/UsersList";
 import RegisterPage from "./pages/RegisterPage";
 import User from "./pages/user";

const App = () => {
  return (
    <BrowserRouter>

      <Routes>

        <Route path="/" element={<Home />} />
         <Route path="/user" element={<User/>} />
        <Route path="/usersList" element={<UsersList/>} />
        <Route path="/LoginPage" element={<LoginPage/>} />
        <Route path="/RegisterPage" element={<RegisterPage />} /> 

      </Routes>

    </BrowserRouter>

  );

};

 

export default App;